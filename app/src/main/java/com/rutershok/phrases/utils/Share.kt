package com.rutershok.phrases.utils

import android.app.Activity
import android.content.*
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.net.Uri
import android.view.View
import android.widget.Toast
import androidx.core.content.FileProvider
import com.rutershok.phrases.R
import com.rutershok.phrases.model.Social
import com.rutershok.phrases.persistence.Storage
import com.rutershok.phrases.persistence.Storage.getPremium
import com.rutershok.phrases.utils.Snackbar.show
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

object Share {
    @JvmStatic
    fun copyToClipboard(activity: Activity, phrase: String?) {
        val stringBuilder = StringBuilder(phrase)
        if (!getPremium(activity)) {
            stringBuilder.append(" ").append(activity.getString(R.string.download_this_app))
        }
        (activity.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager)
            .setPrimaryClip(
                ClipData.newPlainText(
                    activity.getString(R.string.phrases),
                    stringBuilder
                )
            )
        show(activity, R.string.copied_to_clipboard)
    }


    @JvmStatic
    fun withImage(activity: Activity, view: View?) {
        val bitmap = ImageUtil.getScaledBitmapWatermark(activity, view)
        try {
            val cachePath = File(activity.cacheDir, "images")
            if (!cachePath.exists()) {
                cachePath.mkdirs()
            } // don't forget to make the directory
            val outputStream = FileOutputStream("$cachePath/image.png")
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream)
            outputStream.close()
            val newFile = File(File(activity.cacheDir, "images"), "image.png")
            val uri = FileProvider.getUriForFile(activity, Constant.AUTHORITY, newFile)
            if (uri != null) {
                val shareIntent = Intent(Intent.ACTION_SEND)
                    .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    .setDataAndType(uri, activity.contentResolver.getType(uri))
                    .putExtra(Intent.EXTRA_STREAM, uri)
                activity.startActivity(Intent.createChooser(shareIntent, ""))
            }
            if (!bitmap.isRecycled) {
                bitmap.recycle()
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    fun withSocial(activity: Activity, view: View?, social: Social) {
        try {
            val cachePath = File(activity.cacheDir, "images")
            if (!cachePath.exists()) {
                cachePath.mkdirs()
            }
            val outputStream = FileOutputStream("$cachePath/image.png")
            ImageUtil.getScaledBitmapWatermark(activity, view)
                .compress(Bitmap.CompressFormat.PNG, 100, outputStream)
            outputStream.close()
            val newFile = File(File(activity.cacheDir, "images"), "image.png")
            val uri = FileProvider.getUriForFile(activity, Constant.AUTHORITY, newFile)
            if (uri != null) {
                val shareIntent = Intent(Intent.ACTION_SEND)
                    .putExtra(Intent.EXTRA_STREAM, uri)
                    .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    .setType("image/png")
                if (activity.packageManager.getLaunchIntentForPackage(social.packageName) != null) {
                    shareIntent.setPackage(social.packageName)
                    activity.startActivity(Intent.createChooser(shareIntent, ""))
                } else {
                    Toast.makeText(activity, R.string.app_not_installed, Toast.LENGTH_LONG).show()
                }
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    @JvmStatic
    fun openPublisherPage(context: Context) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse("market://search?q=pub:Rutershok")
        context.startActivity(intent)
    }

    @JvmStatic
    fun openAppPage(context: Context, packageName: String) {
        context.startActivity(
            Intent(
                Intent.ACTION_VIEW,
                Uri.parse("market://details?id=$packageName")
            )
        )
    }

    fun launchApp(context: Context, packageName: String) {
        try {
            context.packageManager.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES)
            context.startActivity(context.packageManager.getLaunchIntentForPackage(packageName))
        } catch (e: PackageManager.NameNotFoundException) {
            openAppPage(context, packageName)
        }
    }

    @JvmStatic
    fun openInstagram(context: Context) {
        try {
            context.startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("http://instagram.com/_u/" + Constant.RUTERSHOK)
                ).setPackage("com.instagram.android")
            )
        } catch (e: ActivityNotFoundException) {
            context.startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("http://instagram.com/" + Constant.RUTERSHOK)
                )
            )
        }
    }

    @JvmStatic
    fun openFacebook(context: Context) {
        try {
            context.packageManager.getPackageInfo("com.facebook.katana", 0)
            context.startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("fb://page/" + Constant.FACEBOOK_ID)
                )
            )
        } catch (e: Exception) {
            context.startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://www.facebook.com/" + Constant.RUTERSHOK)
                )
            )
        }
    }

    @JvmStatic
    fun openTwitter(context: Context) {
        val uri = try {
            context.packageManager.getPackageInfo("com.twitter.android", 0)
            Uri.parse("twitter://user?user_id=" + Constant.TWITTER_ID)
        } catch (e: Exception) {
            Uri.parse("https://twitter.com/" + Constant.RUTERSHOK)
        }
        context.startActivity(Intent(Intent.ACTION_VIEW, uri))
    }

    @JvmStatic
    fun contactUs(context: Context) {
        val uri = Uri.parse("mailto:" + Constant.EMAIL_ADDRESS)
            .buildUpon()
            .appendQueryParameter("subject", context.getString(R.string.app_name))
            .build()
        context.startActivity(
            Intent.createChooser(
                Intent(Intent.ACTION_SENDTO, uri), context.getString(
                    R.string.send_mail
                )
            )
        )
    }

    @JvmStatic
    fun shareApp(context: Context) {
        val intent = Intent().setAction(Intent.ACTION_SEND).putExtra(
            Intent.EXTRA_TEXT, context.getString(
                R.string.download_this_app
            )
        ).setType("text/plain")
        context.startActivity(Intent.createChooser(intent, context.getString(R.string.share_with)))
    }

    @JvmStatic
    fun withText(context: Context, phrase: String) {
        if (phrase != "") {
            val phraseText = StringBuilder(phrase)
            if (!Storage.getPremium(context)) {
                phraseText.append(context.getString(R.string.download_this_app))
            }
            context.startActivity(
                Intent.createChooser(
                    Intent()
                        .setAction(Intent.ACTION_SEND)
                        .putExtra(Intent.EXTRA_TEXT, phraseText.toString()).setType("text/plain"),
                    context.getString(R.string.share_with)
                )
            )
        }
    }

    @JvmStatic
    fun copyToClipboard(context: Context, phrase: String) {
        val clipboard = context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText(
            Constant.PHRASE,
            phrase + " " + context.getString(R.string.download_this_app)
        )
        clipboard.setPrimaryClip(clip)
    }

    private fun getBitmap(view: View): Bitmap {
        val bitmap = Bitmap.createBitmap(view.height, view.height, Bitmap.Config.ARGB_8888)
        view.layout(view.left, view.top, view.right, view.bottom)
        view.draw(Canvas(bitmap))
        return Bitmap.createScaledBitmap(bitmap, view.width, view.height, true)
    }

    @JvmStatic
    fun privacyPolicy(context: Context) {
        context.startActivity(
            Intent(
                Intent.ACTION_VIEW,
                Uri.parse("http://www.rutershok.netsons.org/privacy.html")
            )
        )
    }
}