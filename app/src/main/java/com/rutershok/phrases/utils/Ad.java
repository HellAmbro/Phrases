package com.rutershok.phrases.utils;

import android.app.Activity;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;

import com.google.ads.consent.ConsentInfoUpdateListener;
import com.google.ads.consent.ConsentInformation;
import com.google.ads.consent.ConsentStatus;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;
import com.rutershok.phrases.R;
import com.rutershok.phrases.persistence.Storage;

public class Ad {

    private static InterstitialAd mInterstitialAd;
    private static RewardedAd mRewardedAd;
    private static AdRequest mAdRequest;

    private static void getConsentStatus(final Activity activity) {
        if (!activity.isFinishing()) {
            ConsentInformation.getInstance(activity).requestConsentInfoUpdate(new String[]{Constant.ADMOB_PUB_ID}, new ConsentInfoUpdateListener() {
                @Override
                public void onConsentInfoUpdated(ConsentStatus consentStatus) {
                    switch (consentStatus) {
                        case UNKNOWN:
                            Dialog.gdprConsent(activity);
                            break;
                        case NON_PERSONALIZED:
                            ConsentInformation.getInstance(activity).setConsentStatus(ConsentStatus.NON_PERSONALIZED);
                            break;
                        case PERSONALIZED:
                            ConsentInformation.getInstance(activity).setConsentStatus(ConsentStatus.PERSONALIZED);
                            break;
                    }
                }

                @Override
                public void onFailedToUpdateConsentInfo(String errorDescription) {
                }
            });
        }
    }

    public static void initialize(final Activity activity) {
        if (!Storage.getPremium(activity)) {
            getConsentStatus(activity);
            MobileAds.initialize(activity, initializationStatus -> {
            });
            mAdRequest = new AdRequest.Builder().build();
            loadInterstitial(activity);
            loadRewardedVideo(activity);
        }
    }

    public static void showInterstitialWithFrequency(Activity activity) {
        if (!Storage.getPremium(activity) &&
                Storage.getInterstitialCount(activity) % Constant.INTERSTITIAL_FREQUENCY == 0) {
            Snackbar.showText(activity, R.string.loading_ads);
            showInterstitial(activity);
        }
        Storage.updateInterstitialCount(activity);
        Log.e("Count", ""+Storage.getInterstitialCount(activity));
    }

    static void showInterstitial(Activity activity) {
        if (!Storage.getPremium(activity) && mInterstitialAd != null) {
            loadInterstitial(activity);
            if (mInterstitialAd != null) {
                mInterstitialAd.show(activity);
            }
        } else {
            loadInterstitial(activity);
        }
    }

    private static void loadInterstitial(Activity activity) {
        try {
            InterstitialAd.load(activity, Constant.ADMOB_INTERSTITIAL_ID,
                    mAdRequest, new InterstitialAdLoadCallback() {
                        @Override
                        public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                            mInterstitialAd = interstitialAd;
                        }

                        @Override
                        public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                            mInterstitialAd = null;
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void showBanner(final Activity activity) {
        if (!Storage.getPremium(activity)) {
            final AdView bannerView = activity.findViewById(R.id.ads_banner);
            bannerView.loadAd(mAdRequest);
            bannerView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    bannerView.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    static void unlockPremium(final Activity activity) {
        Snackbar.showText(activity, R.string.loading);
        if (mRewardedAd != null) {
            mRewardedAd.show(activity, rewardItem -> Storage.setPremium(activity, true));
        } else {
            Snackbar.showText(activity, R.string.impossible_to_load_video);
            loadRewardedVideo(activity);
        }
    }

    private static void loadRewardedVideo(Activity activity) {
        RewardedAd.load(activity, Constant.ADMOB_REWARDED_VIDEO_ID,
                mAdRequest, new RewardedAdLoadCallback() {
                    @Override
                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                        mRewardedAd = null;
                        Log.e("Error", loadAdError.toString());
                    }

                    @Override
                    public void onAdLoaded(@NonNull RewardedAd rewardedAd) {
                        mRewardedAd = rewardedAd;
                    }
                });
    }
}