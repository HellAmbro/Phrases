package com.rutershok.phrases.utils;

import static android.graphics.Color.WHITE;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.ads.consent.ConsentInformation;
import com.google.ads.consent.ConsentStatus;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.kobakei.ratethisapp.RateThisApp;
import com.rutershok.phrases.R;
import com.rutershok.phrases.adapters.FontsAdapter;
import com.rutershok.phrases.adapters.SocialsAdapter;
import com.rutershok.phrases.model.Font;
import com.rutershok.phrases.model.Target;
import com.rutershok.phrases.persistence.Storage;
import com.xw.repo.BubbleSeekBar;

import java.util.Locale;

public class Dialog {
    public static void showPremiumIsNeeded(Activity activity) {
        new AlertDialog.Builder(activity)
                .setTitle(activity.getString(R.string.premium_version_required))
                .setMessage(activity.getString(R.string.support_our_work_by_purchasing))
                .setPositiveButton(R.string.premium, (dialog, which) -> Dialog.premiumVersion(activity))
                .setNeutralButton(android.R.string.cancel, null)
                .show();
    }
    public static void premiumVersion(final Activity activity) {
        if (!Storage.getPremium(activity)) {
            new AlertDialog.Builder(activity)
                    .setTitle(R.string.purchase_premium_version)
                    .setMessage(R.string.premium_version_body)
                    .setPositiveButton(R.string.purchase, (dialog, which) -> Billing.purchasePremium(activity))
                    .setNegativeButton(R.string.video, (dialog, which) -> Ad.unlockPremium(activity)).setNeutralButton(android.R.string.no, null)
                    .show();
        } else {
            Snackbar.showText(activity, R.string.you_already_have_the_premium_version);
        }
    }

    public static void changeTextColor(Activity activity, final AppCompatEditText textInputEditText) {
        final int currentPhraseColor = textInputEditText.getCurrentTextColor();
        @SuppressLint("InflateParams") View view = LayoutInflater.from(activity).inflate(R.layout.dialog_change_text_color, null, false);
        ColorPickerView colorPickerView = view.findViewById(R.id.color_picker_view);
        colorPickerView.addOnColorChangedListener(textInputEditText::setTextColor);

        new AlertDialog.Builder(activity)
                .setView(view)
                .setIcon(R.drawable.ic_change_text_color)
                .setTitle(R.string.change_text_color)
                .setPositiveButton(android.R.string.ok, null)
                .setNegativeButton(android.R.string.cancel, (dialog, which) -> {
                    //Restore default values
                    textInputEditText.setTextColor(currentPhraseColor);
                }).create().show();
    }

    public static void modifyPhrase(final Activity activity, final AppCompatEditText textInputEditText) {
        final float currentPhraseSize = textInputEditText.getTextSize() / activity.getResources().getDisplayMetrics().scaledDensity;
        final int currentPhraseColor = textInputEditText.getCurrentTextColor();

        //Custom view
        @SuppressLint("InflateParams") View view = LayoutInflater.from(activity).inflate(R.layout.dialog_modify_phrase, null, false);
        final TextInputEditText editTextPhrase = view.findViewById(R.id.edit_text_phrase);
        if (textInputEditText.getText() != null) {
            editTextPhrase.setText(textInputEditText.getText().toString());
            editTextPhrase.setTextColor(textInputEditText.getCurrentTextColor());
            editTextPhrase.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    textInputEditText.setText(s.toString());
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });
        }

        //Text size: [value]
        final TextView textTextSize = view.findViewById(R.id.text_phrase_size);
        textTextSize.setText(String.format(Locale.US, "%s %f", activity.getString(R.string.text_size), currentPhraseSize));
        SeekBar seekBarPhraseSize = view.findViewById(R.id.seek_bar_phrase_size);
        seekBarPhraseSize.setProgress((int) ((currentPhraseSize - 10) / activity.getResources().getDisplayMetrics().scaledDensity));
        seekBarPhraseSize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float newPhraseSize = 10 + progress * activity.getResources().getDisplayMetrics().scaledDensity;
                textInputEditText.setTextSize(newPhraseSize);
                textTextSize.setText(String.format(Locale.US, "%s %f", activity.getString(R.string.text_size), newPhraseSize));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        //Show Dialog
        new AlertDialog.Builder(activity)
                .setView(view)
                .setIcon(R.drawable.ic_modify_phrase)
                .setTitle(R.string.modify_phrase)
                .setPositiveButton(android.R.string.ok, null)
                .setNegativeButton(android.R.string.cancel, (dialog, which) -> {
                    //Restore default values
                    textInputEditText.setTextColor(currentPhraseColor);
                    textInputEditText.setTextSize(currentPhraseSize);
                }).create().show();
    }
    public static void showExit(final Activity activity) {
        new AlertDialog.Builder(activity)
                .setTitle(R.string.exit)
                .setMessage(R.string.do_you_really_want_to_exit)
                .setPositiveButton(android.R.string.ok, (dialog, which) -> activity.finish())
                .setNegativeButton(android.R.string.cancel, null)
                .show();
    }
    public static void changeBackground(final Activity activity, final ImageView imageView) {
        @SuppressLint("InflateParams") View view = LayoutInflater.from(activity).inflate(R.layout.dialog_modify_background, null, false);
        final Drawable currentBackgroundDrawable = imageView.getDrawable();
        final AlertDialog.Builder dialog = new AlertDialog.Builder(activity);

        view.findViewById(R.id.button_choose_photo).setOnClickListener(v -> {
            Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT).setType("image/*");

            Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI).setType("image/*");

            Intent chooserIntent = Intent.createChooser(getIntent, activity.getString(R.string.select_image));
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{pickIntent});

            activity.startActivityForResult(chooserIntent, Constant.PICK_IMAGE);
            dialog.create().dismiss();
        });

        view.findViewById(R.id.button_make_photo).setOnClickListener(v -> {
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            activity.startActivityForResult(cameraIntent, Constant.CAMERA_REQUEST);
            dialog.create().dismiss();
        });

        ColorPickerView colorPickerView = view.findViewById(R.id.color_picker_view);
        colorPickerView.addOnColorChangedListener(selectedColor -> imageView.setImageDrawable(new ColorDrawable(selectedColor)));

        dialog.setView(view)
                .setIcon(R.drawable.ic_change_background)
                .setTitle(R.string.select_image)
                .setPositiveButton(android.R.string.ok, null)
                .setNegativeButton(android.R.string.cancel, (dialog1, which) -> imageView.setImageDrawable(currentBackgroundDrawable)).create();
        //Show Dialog
        dialog.show();
    }
    public static void showEditPhrase(Context context, final TextView textPhrase) {
        @SuppressLint("InflateParams") View view = LayoutInflater.from(context).inflate(R.layout.dialog_edit_phrase, null);
        final TextInputEditText editTextPhrase = view.findViewById(R.id.edit_phrase);
        editTextPhrase.setText(textPhrase.getText());
        new AlertDialog.Builder(context)
                .setView(view)
                .setPositiveButton(android.R.string.yes, (dialog, which) -> {
                    if (editTextPhrase.getText() != null) {
                        textPhrase.setText(editTextPhrase.getText().toString());
                    }
                })
                .setNegativeButton(android.R.string.cancel, null)
                .show();
    }

    public static void modifyFont(final Activity activity, final AppCompatEditText textInputEditText) {
        if (Storage.getPremium(activity)) {
            @SuppressLint("InflateParams") View view = LayoutInflater.from(activity).inflate(R.layout.dialog_modify_font, null, false);
            final Typeface currentTypeface = textInputEditText.getTypeface();
            //Font chooser
            ListView listView = view.findViewById(R.id.listview_fonts);
            listView.setAdapter(new FontsAdapter(activity, Font.getAll(activity), textInputEditText));

            new AlertDialog.Builder(activity)
                    .setView(view)
                    .setIcon(R.drawable.ic_modify_font)
                    .setTitle(R.string.select_font)
                    .setPositiveButton(android.R.string.ok, null)
                    .setNegativeButton(android.R.string.cancel, (dialog, which) -> textInputEditText.setTypeface(currentTypeface)).show();
        } else {
            premiumVersion(activity);
        }
    }

    public static void gdprConsent(final Activity activity) {
        if (!activity.isFinishing()) {
            View view = LayoutInflater.from(activity).inflate(R.layout.dialog_eu_consent, null, false);
            new AlertDialog.Builder(activity)
                    .setTitle(R.string.data_protection_consent)
                    .setIcon(R.drawable.ic_data_protection_consent)
                    .setView(view)
                    .setPositiveButton(android.R.string.yes, (dialog, which) -> ConsentInformation.getInstance(activity).setConsentStatus(ConsentStatus.PERSONALIZED)).setNegativeButton(android.R.string.no, (dialog, which) -> ConsentInformation.getInstance(activity).setConsentStatus(ConsentStatus.NON_PERSONALIZED)).show();
        }
    }

    public static void showTutorial(Activity activity) {
        if (!Storage.getIsShowed(activity, Constant.TUTORIAL)) {
            new TapTargetSequence(activity).targets(Target.getAll(activity))
                    .start();
            Storage.setIsShowed(activity, Constant.TUTORIAL, true);
        }
    }

    public static void rateThisApp(Activity activity) {
        RateThisApp.onCreate(activity);
        RateThisApp.showRateDialogIfNeeded(activity);
    }


    public static class ShareBottomSheet extends BottomSheetDialog {

        final Activity mActivity;

        public ShareBottomSheet(@NonNull Activity activity, String phrase, View viewPhrase) {
            super(activity);
            this.mActivity = activity;
            initialize(phrase, viewPhrase);
        }

        private void initialize(String phrase, View viewPhrase) {
            @SuppressLint("InflateParams") View view = getLayoutInflater().inflate(R.layout.bottom_sheet_share, null);
            setContentView(view);
            setShareWithSocial(view, viewPhrase);
            TabLayout tabLayout = view.findViewById(R.id.tab_layout_share);
            tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    switch (tab.getPosition()) {
                        case 0:
                            Share.withImage(mActivity, viewPhrase);
                            break;
                        case 1:
                            Share.withText(mActivity, phrase);
                            break;
                    }
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {
                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {
                    switch (tab.getPosition()) {
                        case 0:
                            Share.withImage(mActivity, viewPhrase);
                            break;
                        case 1:
                            Share.withText(mActivity, phrase);
                            break;
                    }
                }
            });
        }

        private void setShareWithSocial(View view, View viewPhrase) {
            RecyclerView recyclerView = view.findViewById(R.id.recycler_socials);
            recyclerView.setAdapter(new SocialsAdapter(mActivity, viewPhrase));
            recyclerView.setHasFixedSize(true);
        }
    }


    private static GradientDrawable gradientDrawable;

    private static int gradientColorTop = WHITE;
    private static int gradientColorBottom = WHITE;

    public static void showChangeTextColor(final Context context, final TextView textView) {
        int currentTextColor = textView.getCurrentTextColor();
        ColorPickerDialogBuilder
                .with(context)
                .setTitle(context.getString(R.string.choose_color))
                .setOnColorChangedListener(textView::setTextColor)
                .setPositiveButton(android.R.string.yes, (dialogInterface, i, integers) -> {/*Don't remove this*/})
                .setNegativeButton(android.R.string.cancel, (dialog, which) -> textView.setTextColor(currentTextColor))
                .build()
                .show();
    }

    public static void showChangeTextOpacity(Context context, final TextView textView) {
        final float opacity = textView.getAlpha();
        new SeekBarBottomSheet(context, (int) (opacity * 100), new BubbleSeekBar.OnProgressChangedListener() {
            @Override
            public void onProgressChanged(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat, boolean fromUser) {
                textView.setAlpha(progress * 0.01f);
            }

            @Override
            public void getProgressOnActionUp(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {
            }

            @Override
            public void getProgressOnFinally(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat, boolean fromUser) {
            }
        });
    }

    public static void showChangeTextSpacing(Context context, final TextView textView) {
        new SeekBarBottomSheet(context, (int) (textView.getLineSpacingMultiplier() / 0.02), new BubbleSeekBar.OnProgressChangedListener() {
            @Override
            public void onProgressChanged(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat, boolean fromUser) {
                textView.setLineSpacing(0, progress * 0.02f);
            }

            @Override
            public void getProgressOnActionUp(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {
            }

            @Override
            public void getProgressOnFinally(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat, boolean fromUser) {
            }
        });
    }

    public static void showChangeTextHighlight(final Context context, final TextView textView) {
        final Drawable background = textView.getBackground();
        ColorPickerDialogBuilder.with(context)
                .setTitle(context.getString(R.string.choose_color))
                .setOnColorChangedListener(selectedColor -> textView.setBackground(new ColorDrawable(selectedColor)))
                .setPositiveButton(android.R.string.yes, (dialogInterface, i, integers) -> {/*Don't remove this*/})
                .setNegativeButton(android.R.string.cancel, (dialog, which) -> textView.setBackground(background))
                .build()
                .show();
    }

    public static void showChangeTextBorder(Context context, final TextView textView) {
        final RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) textView.getLayoutParams();
        final int currentMargin = layoutParams.leftMargin;

        new SeekBarBottomSheet(context, currentMargin / 4, new BubbleSeekBar.OnProgressChangedListener() {
            @Override
            public void onProgressChanged(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat, boolean fromUser) {
                ViewGroup.MarginLayoutParams marginParams = new ViewGroup.MarginLayoutParams(textView.getLayoutParams());
                int margin = progress * 4;
                marginParams.setMargins(margin, 0, margin, 0);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(marginParams);
                layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
                textView.setLayoutParams(layoutParams);
            }

            @Override
            public void getProgressOnActionUp(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {

            }

            @Override
            public void getProgressOnFinally(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat, boolean fromUser) {

            }
        });
    }

    public static void showChangeBackgroundColor(final Context context, final ImageView imageView) {
        Drawable currentBackgroundDrawable = imageView.getDrawable();
        ColorPickerDialogBuilder
                .with(context)
                .setTitle(context.getString(R.string.choose_color))
                .setOnColorChangedListener(selectedColor -> Glide.with(context).load(new ColorDrawable(selectedColor)).into(imageView))
                .setPositiveButton(android.R.string.yes, (dialogInterface, i, integers) -> {/*Don't remove this*/})
                .setNegativeButton(android.R.string.cancel, (dialog, which) -> Glide.with(context).load(currentBackgroundDrawable).into(imageView))
                .build()
                .show();
    }

    public static void showChangeBackgroundGradient(Context context, ImageView imageView) {
        final Drawable currentBackgroundDrawable = imageView.getDrawable();
        gradientDrawable = new GradientDrawable();
        gradientDrawable.setCornerRadius(0f);
        gradientDrawable.setOrientation(GradientDrawable.Orientation.BOTTOM_TOP);
        try {
            //Bottom color
            ColorPickerDialogBuilder
                    .with(context)
                    .setTitle(context.getString(R.string.choose_color))
                    .wheelType(ColorPickerView.WHEEL_TYPE.CIRCLE)
                    .density(8)
                    .setOnColorChangedListener(selectedColor -> {
                        gradientColorBottom = selectedColor;
                        gradientDrawable.setColors(new int[]{gradientColorBottom, gradientColorTop});
                        Glide.with(context).load(gradientDrawable).into(imageView);
                    })
                    .setPositiveButton(android.R.string.yes, (dialog, selectedColor, allColors) -> {
                        //Top color
                        ColorPickerDialogBuilder
                                .with(context)
                                .setTitle(context.getString(R.string.choose_color))
                                .wheelType(ColorPickerView.WHEEL_TYPE.CIRCLE)
                                .density(8)
                                .setOnColorChangedListener(selectedColor1 -> {
                                    gradientColorTop = selectedColor1;
                                    gradientDrawable.setColors(new int[]{gradientColorBottom, gradientColorTop});
                                    Glide.with(context).load(gradientDrawable).into(imageView);
                                })
                                .setPositiveButton(android.R.string.yes, (dialogInterface, i, integers) -> {
                                })
                                .setNegativeButton(android.R.string.cancel, (dialog1, which) -> Glide.with(context).load(currentBackgroundDrawable).into(imageView))
                                .build()
                                .show();
                    })
                    .setNegativeButton(android.R.string.cancel, (dialog, which) -> Glide.with(context).load(currentBackgroundDrawable).into(imageView))
                    .build()
                    .show();
        } catch (Exception e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    public static void showChangeBackgroundOpacity(Context context, View view) {
        float opacity = view.getAlpha();
        new SeekBarBottomSheet(context, (int) (opacity * 100), new BubbleSeekBar.OnProgressChangedListener() {
            @Override
            public void onProgressChanged(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat, boolean fromUser) {
                view.setAlpha(progress * 0.01f);
            }

            @Override
            public void getProgressOnActionUp(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {

            }

            @Override
            public void getProgressOnFinally(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat, boolean fromUser) {

            }
        }).show();
    }

    public static void showChangeTextSize(Context context, TextView textView) {
        int currentSize = (int) (textView.getTextSize() / context.getResources().getDisplayMetrics().scaledDensity);
        new SeekBarBottomSheet(context, currentSize, new BubbleSeekBar.OnProgressChangedListener() {
            @Override
            public void onProgressChanged(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat, boolean fromUser) {
                textView.setTextSize(progress);
            }

            @Override
            public void getProgressOnActionUp(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {

            }

            @Override
            public void getProgressOnFinally(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat, boolean fromUser) {

            }
        });
    }

    private static class SeekBarBottomSheet extends BottomSheetDialog {
        SeekBarBottomSheet(Context context, int progress, BubbleSeekBar.OnProgressChangedListener listener) {
            super(context);

            @SuppressLint("InflateParams") View view = getLayoutInflater().inflate(R.layout.bottom_sheet_seek_bar, null);
            setContentView(view);

            BubbleSeekBar seekBar = view.findViewById(R.id.bubble_seek_bar);
            seekBar.setProgress(progress);
            seekBar.setOnProgressChangedListener(listener);

            show();
        }

    }

}