package com.rutershok.phrases.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.rutershok.daily.adapters.editor.ImagesAdapter;
import com.rutershok.phrases.R;
import com.rutershok.phrases.adapters.editor.BackgroundEditorAdapter;


public class EditBackgroundFragment extends Fragment {

    private Activity mActivity;
    private ImageView mImageView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        return getLayoutInflater().inflate(R.layout.fragment_edit_background, null);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null) {
            mActivity = getActivity();
            mImageView = mActivity.findViewById(R.id.image_background);
        }
        ((RecyclerView) view.findViewById(R.id.recycler_background_editors)).setAdapter(new BackgroundEditorAdapter(mActivity));
        ((RecyclerView) view.findViewById(R.id.recycler_images)).setAdapter(new ImagesAdapter(view.getContext(), mImageView));
    }
}
