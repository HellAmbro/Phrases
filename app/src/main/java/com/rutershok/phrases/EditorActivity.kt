package com.rutershok.phrases

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.rutershok.phrases.adapters.editor.EditorPagerAdapter
import com.rutershok.phrases.persistence.Storage
import com.rutershok.phrases.utils.Ad
import com.rutershok.phrases.utils.Constant
import com.rutershok.phrases.utils.Dialog
import com.rutershok.phrases.utils.ImageUtil

class EditorActivity : AppCompatActivity() {
    private var mImageBackground: ImageView? = null
    private var mTextPhrase: TextView? = null
    private var mImageLogo: ImageView? = null
    private var mPhrase: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_editor)
        title = getString(R.string.editor)
        mPhrase = intent.getSerializableExtra(Constant.PHRASE) as String?
        if (null != supportActionBar) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }

        initText()
        initImage()
        Ad.showBanner(this)
        findViewById<ViewPager>(R.id.pager_editor).adapter = EditorPagerAdapter(supportFragmentManager, this)

    }

    //Used inside onTouchListener in textView
    private var mX = 0f
    private var mY = 0f
    private var mLastAction = 0
    private fun initText() {
        mTextPhrase = findViewById(R.id.text_phrase)
        //Get text from other apps
        val extraText = intent.getStringExtra(Intent.EXTRA_TEXT)
        if (mPhrase != null) {
            mTextPhrase!!.text = mPhrase
        } else if (extraText != null) {
            mTextPhrase!!.text = extraText
        }
        mTextPhrase!!.setOnTouchListener { v: View, event: MotionEvent ->
            when (event.action) {
                MotionEvent.ACTION_UP -> {
                    if (mLastAction == MotionEvent.ACTION_DOWN) {
                        Dialog.showEditPhrase(v.context, mTextPhrase)
                        mLastAction = event.action
                    }
                    v.performClick()
                }
                MotionEvent.ACTION_DOWN -> {
                    mX = v.x - event.rawX
                    mY = v.y - event.rawY
                    mLastAction = event.action
                }
                MotionEvent.ACTION_MOVE -> {
                    v.animate().x(event.rawX + mX).y(event.rawY + mY).setDuration(0).start()
                    mLastAction = event.action
                }
                else -> v.performClick()
            }
            true
        }
        mTextPhrase!!.setOnLongClickListener { v: View ->
            Dialog.showEditPhrase(v.context, mTextPhrase)
            false
        }
    }

    private fun initImage() {
        mImageBackground = findViewById(R.id.image_background)
        mImageLogo = findViewById(R.id.image_logo)
        (mImageLogo as ImageView).setOnClickListener {
            Dialog.showPremiumIsNeeded(
                this
            )
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> Dialog.showExit(this)
            R.id.action_edit_phrase -> Dialog.showEditPhrase(this, mTextPhrase)
            R.id.action_save -> ImageUtil.saveImage(
                this,
                ImageUtil.getScaledBitmap(findViewById(R.id.relative_phrase_editor))
            )
            R.id.action_share -> Dialog.ShareBottomSheet(
                this,
                mPhrase,
                findViewById(R.id.relative_phrase_editor)
            ).show()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_editor, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (null != data && null != data.data) {
            Glide.with(this).load(data.data)
                .apply(RequestOptions().centerCrop())
                .skipMemoryCache(true)
                .into(mImageBackground!!)
        }
    }

    override fun onResume() {
        super.onResume()
        if (Storage.getPremium(this)) {
            mImageLogo!!.visibility = View.GONE
        }
    }

    override fun onBackPressed() {
        Dialog.showExit(this)
    }
}