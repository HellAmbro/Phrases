package com.rutershok.phrases

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.MenuItemCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.rutershok.phrases.adapters.PhrasesAdapter
import com.rutershok.phrases.persistence.Storage
import com.rutershok.phrases.persistence.Storage.getLanguage
import com.rutershok.phrases.utils.Ad
import com.rutershok.phrases.utils.Constant
import com.rutershok.phrases.utils.Dialog
import com.rutershok.phrases.utils.Snackbar.networkUnavailable
import com.rutershok.phrases.utils.Snackbar.showText
import org.json.JSONException
import org.json.JSONObject
import java.net.URLEncoder
import java.util.*


class PhrasesActivity : AppCompatActivity() {
    private val mPhrasesList = ArrayList<String>()
    private val mPhrasesAdapter = PhrasesAdapter(mPhrasesList)
    private var categoryName: String? = null
    private var categoryKey: String? = null
    private var search: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_phrases)
        categoryName = intent.getStringExtra(Constant.CATEGORY_NAME)
        categoryKey = intent.getStringExtra(Constant.CATEGORY_KEY)
        setActionBar()
        setRecyclerView()
        Ad.showBanner(this)
        Ad.showInterstitialWithFrequency(this)
        loadPhrases()
    }

    private fun setActionBar() {
        if (supportActionBar != null) {
            supportActionBar!!.title = categoryName
            supportActionBar!!.setDisplayShowHomeEnabled(true)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setHomeButtonEnabled(true)
        }
    }

    private fun setRecyclerView() {
        val recyclerView = findViewById<RecyclerView>(R.id.recycler_phrases)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = mPhrasesAdapter
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (!recyclerView.canScrollVertically(1)) {
                    loadPhrases()
                    Ad.showInterstitialWithFrequency(this@PhrasesActivity)
                }
            }
        })
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun loadPhrases() {
        val progressBar = findViewById<ProgressBar>(R.id.pb_loading_phrases)
        progressBar.visibility = View.VISIBLE
        val url = (Constant.API_URL + "get_phrases_v2.php"
                + "?language=" + getLanguage(this)
                + "&category=" + URLEncoder.encode(categoryKey, "UTF-8")
                + "&search=" + URLEncoder.encode(search, "UTF-8")
                + "&from=" + mPhrasesList.size
                + "&limit=" + Constant.LIMIT)
        Log.e("URL", url)
        Volley.newRequestQueue(this).add(StringRequest(
            Request.Method.GET, url,
            { response: String ->
                try {
                    val jsonArray = JSONObject(response).getJSONArray(Constant.PHRASES)
                    for (i in 0 until jsonArray.length()) {
                        mPhrasesList.add(jsonArray.getJSONObject(i).getString(Constant.PHRASE))
                        mPhrasesAdapter.notifyDataSetChanged()
                    }
                    if (jsonArray.length() == 0) {
                        showText(this@PhrasesActivity, R.string.phrases_ended)
                        progressBar.visibility = View.GONE
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                } finally {
                    progressBar.visibility = View.GONE
                }
            }) {
            progressBar.visibility = View.GONE
            networkUnavailable(
                this@PhrasesActivity,
                View.OnClickListener { loadPhrases() })
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_search, menu)
        val searchViewItem: MenuItem = menu.findItem(R.id.app_bar_search)

        val searchView: SearchView = MenuItemCompat.getActionView(searchViewItem) as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (Storage.getPremium(this@PhrasesActivity)) {
                    search = query?.lowercase().toString()
                    mPhrasesList.clear()
                    loadPhrases()
                } else {
                    Dialog.showPremiumIsNeeded(this@PhrasesActivity)
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                search = newText?.lowercase().toString()
                if (search == "") {
                    mPhrasesList.clear()
                    loadPhrases()
                }
                /*search = newText?.lowercase().toString()
                mPhrasesList.clear()
                loadPhrases()*/
                return false
            }
        })
        return super.onCreateOptionsMenu(menu)
    }
}